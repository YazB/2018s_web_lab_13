package ictgradschool.web.lab13.ex1;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

public class Exercise01 {

    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */

        Properties dbProps = new Properties();
        //Want to add String pass for practice - what is it though??
        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");

            while(true) {
                System.out.println("Please input partial title of article: ");
                Scanner keyboard = new Scanner(System.in);
                String partialArticleTitle = keyboard.nextLine();

                if(partialArticleTitle.equalsIgnoreCase("q")) {
                    break;
                }

                //Anything that requires the use of the connection should be in here...
                try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM lab13_articles WHERE title LIKE ?;")) {
                    stmt.setString(1, "%"+partialArticleTitle + "%");
//                stmt.setString(2, partialArticleTitle);

                    try (ResultSet r = stmt.executeQuery()) {
                        while (r.next()) {
                            String a = r.getString(1);
                            String b = r.getString(2);
                            String c = r.getString(r.findColumn("body"));
                            System.out.println(a);
                            System.out.println(b);
                            System.out.println(c);
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
