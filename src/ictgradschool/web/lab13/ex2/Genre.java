package ictgradschool.web.lab13.ex2;

public class Genre {
    private final String name;

    public Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
