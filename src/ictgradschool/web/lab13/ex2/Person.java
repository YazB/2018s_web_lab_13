package ictgradschool.web.lab13.ex2;

public class Person {
    private final String name;


    public Person(String role_name) {
        this.name =role_name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
