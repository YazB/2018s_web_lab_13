package ictgradschool.web.lab13.ex2;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;


public class DatabaseAccess {
    private static Properties dbProps = new Properties();

    static {
        try (FileReader fr = new FileReader("mysql.properties")) {
            dbProps.load(fr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List <FilmRolePair> getFilmAndRoleInformationForParticipatingActor(String entry) {
        List <FilmRolePair> entries = new ArrayList <>();

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement p = conn.prepareStatement("SELECT f.film_title, r.role_name FROM pfilms_film AS f, pfilms_actor AS a, pfilms_role AS r, pfilms_participates_in AS p WHERE p.actor_id = a.actor_id AND p.film_id = f.film_id AND p.role_id = r.role_id AND (actor_fname LIKE ? OR actor_lname LIKE ?)")) {
                p.setString(1, "%" + entry + "%");
                p.setString(2, "%" + entry + "%");
                try (ResultSet rs = p.executeQuery()) {
                    while (rs.next()) {
                        Film f = new Film(rs.getString(1));
                        Role r = new Role(rs.getString(2));
                        //1 actor name
                        //2 film title
                        //3 role name

                        entries.add(new FilmRolePair(f, r));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entries;
    }

    //Get actor name in different list
    //Run through as above and get query
    //Would have to do with all of them!

    public static String getActorInformationForParticipatingActor(String entry) {
        String actorName = null;

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement p = conn.prepareStatement("SELECT CONCAT(actor_fname, ' ', actor_lname) AS actor_name FROM pfilms_actor WHERE actor_fname LIKE ? OR actor_lname LIKE ?")) {
                p.setString(1, "%" + entry + "%");
                p.setString(2, "%" + entry + "%");
                try (ResultSet rs = p.executeQuery()) {
                    while (rs.next()) {
                       String a = (rs.getString(1));
                        actorName = a;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actorName;
    }


    public static List <PersonRolePair> getPersonAndRoleInformationForFilm(String entry) {
        List <PersonRolePair> entries = new ArrayList <>();

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement p = conn.prepareStatement("SELECT CONCAT(a.actor_fname, ' ', a.actor_lname) AS name, f.film_title AS title, r.role_name AS role, f.genre_name AS genre FROM pfilms_film AS f, pfilms_actor AS a, pfilms_role AS r, pfilms_participates_in AS p WHERE f.film_title =? AND f.film_id = p.film_id AND p.actor_id = a.actor_id AND p.role_id = r.role_id")) {
                p.setString(1, "%" + entry + "%");


                try (ResultSet rs = p.executeQuery()) {

                    while (rs.next()) {
                        System.out.println("found resujlt?????");
                        Person person = new Person(rs.getString(1));
                        System.out.println(rs.getString(1));
                        Role r = new Role(rs.getString(2));
                        entries.add(new PersonRolePair(person, r));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entries;
    }

    //Get film name in different list
    //Run through as above and get query
    //Would have to do with all of them!


    public static List <Film> getFilmForParticipatingPersonAndRole(String entry) {
        List <Film> entries = new ArrayList <>();

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement p = conn.prepareStatement("SELECT film_title FROM pfilms_film WHERE film_title LIKE ?")) {
                p.setString(1, "%" + entry + "%");
                try (ResultSet rs = p.executeQuery()) {
                    while (rs.next()) {
                        String f = (rs.getString(2));
                        entries.add(new Film(f));
                        //Need new constructor on Actor front for fname, lname, need to return pairs
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entries;
    }


    public static List <String> getGenreAndFilmInformationforGenre(String entry) {
        List <String> entries = new ArrayList <>();

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement p = conn.prepareStatement("SELECT f.film_title FROM pfilms_film AS f, pfilms_genre AS g, pfilms_participates_in AS p WHERE f.film_id = p.film_id AND f.genre_name = g.genre_name AND g.genre_name LIKE ?")) {
                p.setString(1, "%" + entry + "%");

                try (ResultSet rs = p.executeQuery()) {
                    while (rs.next()) {
                        String f = (rs.getString(1));
                        entries.add(f);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entries;
    }
    //Get film genre in different list
    //Run through as above and get query
    //Would have to do with all of them!

//    public static String getFilmGenreForParticipatingActor(String entry) {
//        String genreName = null;
//
//        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
//            try (PreparedStatement p = conn.prepareStatement("SELECT genre_name FROM pfilms_genre  WHERE genre_name LIKE ? AND actor_name LIKE ?")) {
//                p.setString(1, "%" + entry + "%");
//                try (ResultSet rs = p.executeQuery()) {
//                    while (rs.next()) {
//                        String x = (rs.getString(1));
//                        genreName = x;
//                        //Need new constructor on Actor front for fname, lname, need to return pairs
//                    }
//                }
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return genreName;
//    }



}

