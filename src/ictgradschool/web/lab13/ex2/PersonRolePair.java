package ictgradschool.web.lab13.ex2;

public class PersonRolePair {
    private Person p;
    private Role r;

    public PersonRolePair(Person p, Role r) {
        this.p = p;
        this.r = r;
    }

    public Person getP() {
        return p;
    }

    public Role getR() {
        return r;
    }

    @Override
    public String toString() {
        return p + " (" + r + ")";
    }
}
