package ictgradschool.web.lab13.ex2;

public class FilmRolePair {
    private Film f;
    private Role r;

    public FilmRolePair(Film f, Role r) {
        this.f = f;
        this.r = r;
    }

    public Film getFilm() {
        return f;
    }

    public Role getRole() {
        return r;
    }

    @Override
    public String toString() {
        return f + " (" + r + ")";
    }
}
