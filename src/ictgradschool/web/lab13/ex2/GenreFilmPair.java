package ictgradschool.web.lab13.ex2;

public class GenreFilmPair {
    private Genre g;
    private Film f;

    public GenreFilmPair(Genre g, Film f) {
        this.g = g;
        this.f =f;
    }

    public Genre getG() {
        return g;
    }

    public Film getF() {
        return f;
    }

    @Override
    public String toString() {
        return g.toString();

    }
}
