package ictgradschool.web.lab13.ex2;

public class Producer {
    private final String name;

    public Producer(String producer_name) {
        this.name = producer_name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
