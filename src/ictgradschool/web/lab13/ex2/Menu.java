package ictgradschool.web.lab13.ex2;

import java.util.List;
import java.util.Scanner;

public class Menu {
    private final int ACTOR = 1;
    private final int FILM = 2;
    private final int GENRE = 3;
    private final int QUIT = 4;
    private final String RETURN_FROM_MENU = "";


    public static void main(String[] args) {
        new Menu().start();
    }

    public void menuActor(Scanner in) {


        while (true) {

            System.out.println("Please enter the name of the actor you wish to\n" +
                    "get information about, or press enter to return\n" +
                    "to the previous menu");

            String entry = in.nextLine();

            if (entry.equalsIgnoreCase(RETURN_FROM_MENU)) {
                return;
            }
            //Get actor name first and save into string e.g. String name = DatabaseAccess.get ....
            List <FilmRolePair> actors = DatabaseAccess.getFilmAndRoleInformationForParticipatingActor(entry);

            String fullName = DatabaseAccess.getActorInformationForParticipatingActor(entry);

            if (actors.size() == 0) {
                System.out.println("Sorry, we couldn't find any actor by that name.");
                System.out.println("Please enter the name of the actor you wish to get information about, or press enter to return to the previous menu.");
                //Commands here??
            } else {
                //Instead of entry in below line, name
                System.out.println(fullName + " is listed as being involved in the following films:");
                System.out.println(" ");
                for (FilmRolePair actor : actors) {
                    System.out.println(actors);
                    System.out.println();
                }
            }
        }
    }

    //Include same comments as above for other queries!!!!!

    public void menuFilm(Scanner in) {


        while (true) {

            System.out.println("Please enter the name of the film you wish to\n" +
                    "get information about, or press enter to return\n" +
                    "to the previous menu");

            String entry = in.nextLine();

            if (entry.equalsIgnoreCase(RETURN_FROM_MENU)) {
                return;
            }
            List<PersonRolePair> films =DatabaseAccess.getPersonAndRoleInformationForFilm(entry);

            if(films.size()==0) {
                System.out.println("Sorry, we couldn't find any film by that name.");
                System.out.println("Please enter the name of the film you wish to get information about, or press enter to return to the previous menu.");
                //Commands here??
            } else {
                System.out.println(entry + " is a " + entry +" movie that features the following people: ");
                //Second entry in above line should be giving genre, not film title!!

                System.out.println(" ");
                for(PersonRolePair film: films) {
                    System.out.println(films);
                    System.out.println();
                }
            }
        }
    }

    public void menuGenre(Scanner in) {



        while (true) {

            System.out.println("Please enter the name of the genre you wish to\n" +
                    "get information about, or press enter to return\n" +
                    "to the previous menu");

            String entry = in.nextLine();

            if (entry.equalsIgnoreCase(RETURN_FROM_MENU)) {
                return;
            }
            List<String> genres = DatabaseAccess.getGenreAndFilmInformationforGenre(entry);

            if(genres.size()==0) {
                System.out.println("Sorry, we couldn't find any genre by that name");
                System.out.println("Please enter the name of the genre you wish to get information about, or press enter to return to the previous menu.");
                //Commands here???
            } else {
                System.out.println("The" + entry + " genre includes the following films:");
                System.out.println(" ");
                for(String genre: genres) {
                    System.out.println(genres);
                    System.out.println();
                }
            }
        }
    }

    public void start() {
        System.out.println("Welcome to the Film database!");
        System.out.println();

        Scanner in = new Scanner(System.in);

        while (true) {
            System.out.println("Please select an option from the following:");
            System.out.println(ACTOR + ". Information by Actor");
            System.out.println(FILM + ". Information by Movie");
            System.out.println(GENRE + ". Information by Genre");
            System.out.println(QUIT + ". Exit");
            System.out.print("\n> ");


            int choose = Integer.valueOf(in.nextLine());

            if (choose == ACTOR) {
                menuActor(in);
            } else if (choose == FILM) {
                menuFilm(in);
            } else if (choose == GENRE) {
                menuGenre(in);
            } else if (choose == QUIT) {
                System.out.println("Goodbye!");
                return;
            }
        }
    }
}
