package ictgradschool.web.lab13.ex2;

public class Film {
    private final String name;

    public Film(String film_name) {
        this.name = film_name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
